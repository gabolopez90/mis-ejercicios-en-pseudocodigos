Algoritmo Ejercicio_5
//	Programa que pida el total de kil�metros recorridos
//	el precio de la gasolina (por litro) y 
//	el tiempo que se ha tardado (en horas y minutos) 
//	y que calcule: 
//	Consumo de gasolina (en litros y Bs.) en el recorrido.
//	Velocidad media (en km/h y m/s).
//	Este veh�culo ofrece un rendimiento promedio de 16 kil�metros por litro.
	
	Escribir "Ingrese Km recorridos"
	Leer km
	Escribir "Ingrese precio de gasolina (por litro)"
	Leer precio
	Escribir "Ingrese tiempo en llegar a destino (horas)"
	Leer horas
	Escribir "Ingrese tiempo en llegar a destino (minutos)"
	Leer minutos
	
	rendimiento = 16	
	horastotal = horas + (minutos/60)
	litros = km / rendimiento
	bs = precio * litros
	mediakm = km / horastotal
	mediamin = mediakm / 3.6
	
	Escribir "Consumo de " litros " litros de gasolina"
	Escribir  "Consumo de Bs " bs " en gasolina"
	Escribir "Velocidad media " mediakm " km/h"
	Escribir  "Velocidad media " mediamin " m/s"
	
FinAlgoritmo
